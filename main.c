#include <stdio.h>
#include "prefix.h"

int main() {
    // Test adding prefixes
    add(0xC0A80000, 24); // 192.168.0.0/24
    add(0x0A000000, 8);  // 10.0.0.0/8
    add(0xAC100000, 13); // 172.16.0.0/13
    add(0xC0000200, 24); // 192.0.2.0/24
    add(0xCB007100, 25); // 203.0.113.0/29
    add(0xAC1F0000, 16); // 172.31.0.0/16
    add(0xC0A80100, 24); // 192.168.1.0/24
    add(0x0A0A0000, 16); // 10.10.0.0/16
    add(0xC6336400, 24); // 198.51.100.0/24
    add(0xAC140000, 16); // 172.20.0.0/16
    add(0xAC100000, 13); // 172.16.0.0/13 - already added
    add(0xAC100000, 33); // 172.16.0.0/33 - mask out of the range
    printPrefixes(head);

    // Test deleting prefixes
    del(0xC0A80100, 24); // 192.168.1.0/24
    del(0x0A000000, 8);  // 10.0.0.0/8
    del(0xC0A80032, 23); // 192.168.0.50/23 - non existing prefix
    printPrefixes(head);

    // Test checking IP address
    printf("Check 00.00.00.00: %d\n", check(0x00000000));     // Should return -1 (not contained)
    printf("Check 255.255.255.255: %d\n", check(0xFFFFFFFF)); // Should return -1 (not contained)
    printf("Check 192.168.0.50: %d\n", check(0xC0A80032));    // Should return 24 (192.168.0.0/24)
    printf("Check 10.15.0.100: %d\n", check(0x0A0F0064));     // Should return -1 (not contained)
    printf("Check 172.16.10.200: %d\n", check(0xAC100AC8));   // Should return 13 (not contained)
    printf("Check 203.0.113.25: %d\n", check(0xCB007119));    // Should return 25 (203.0.113.0/25)
    printf("Check 192.168.1.75: %d\n", check(0xC0A8014B));    // Should return -1 (not contained)
    printf("Check 172.19.5.10: %d\n", check(0xAC13050A));     // Should return 13 (not contained)

    return 0;
}
