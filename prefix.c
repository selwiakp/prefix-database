#include <stdio.h>
#include <stdlib.h>
#include "prefix.h"

PrefixCollection * head = NULL;


// Function which add a new prefix
// Returns 0 if everything is ok or -1 is there is an error
int add(unsigned int base, char mask) {
    int status = 0;

    // Check if given data are correct or prefix exist
    if ((-1 == checkData(base, mask)) || (-1 == checkPrefix(head, base, mask))) {
        status = -1;
    } else {
        //add new node
        insertNode(&head, base, mask);
    }

    return status;
}

// Function which delete a prefix
// Returns 0 if everything is ok or -1 is there is an error
int del(unsigned int base, char mask) {
    int status = 0;

    // Check if given data are correct
    if (-1 == checkData(base, mask)) {
        status = -1;
    } else {
        // delete selected node
        deleteNode(&head, base, mask);
    }

    return status;
}


// Function checks if a given IP address is contained in the set of prefixes
// Returns -1 if an IP is not contained or lagest mask number from found prefix
char check(unsigned int ip) {
    char max_mask = -1;
    PrefixCollection* node = head;

    while (NULL != node) {
        // create mask of ones and move it to the left
        unsigned int netmask = ~0 << (32 - node->prefixes.mask);
        // AND operation to extract network adress
        unsigned int network = node->prefixes.base & netmask;

        // check if prefix contain adress and if current mask is greater than saved one
        if ((network == (ip & netmask)) && (max_mask < (node->prefixes.mask))) {
            max_mask = node->prefixes.mask;
        }

        // set the next node to search, left if ip is smaller than network, right if ip is greater than network
        if (ip < network) {
            node = node->left;
        } else {
            node = node->right;
        }
    }

    return max_mask;
}

// Function which checks if given data are correct
// Returns 0 for correct data or -1 for incorrect
int checkData(unsigned int base, char mask) {
    int status = 0;

    // Check if given data are correct
    if ((mask < 0) || (mask > 32)) {
        status = -1;
    }

    return status;
}

// Function which checks if given prefix already exist
// Returns 0 if prefix not exist or -1 if exist
int checkPrefix(PrefixCollection* node, unsigned int base, char mask) {
    int status = 0;

    // Check if the prefix already exists
    if (NULL == node) {
        // prefix not exist
        status = 0;
    } else if(base == (node->prefixes.base) && (mask == (node->prefixes.mask))) {
        // Prefix already exists
        status = -1;
    } else if (base < (node->prefixes.base)) {
        // set the next node to search, left if given base is smaller than currently checking prefix base
        status = checkPrefix(node->left, base, mask);
    } else {
        // set the next node to search, right if given base is greater than currently checking prefix base
        status = checkPrefix(node->right, base, mask);
    }

    return status;
}

// Function which print all existing prefixes
void printPrefixes(PrefixCollection* node) {
    if (NULL == node) {
        //do nothing
    } else {
    printPrefixes(node->left);
    printf("%x/%d\n", node->prefixes.base, node->prefixes.mask);
    printPrefixes(node->right);
    }

    return;
}

// Function which insert new node
void insertNode(PrefixCollection** node, unsigned int base, char mask) {
    // Check if the node is empty
    if (NULL == *node) {
        // Create a new node
        *node = (PrefixCollection*)malloc(sizeof(PrefixCollection));
        (*node)->prefixes.base = base;
        (*node)->prefixes.mask = mask;
        (*node)->left = NULL;
        (*node)->right = NULL;
    } else {
        //find correct position for node
        if (base < ((*node)->prefixes.base)) {
            //on the left side if given base is smaller than parrent
            insertNode(&((*node)->left), base, mask);
        } else {
            //on the right side if given base is greater than parrent
            insertNode(&((*node)->right), base, mask);
        }
    }

    return;
}

// Function which delete selected node
// Returns 0 if the node was successfully deleted, -1 otherwise
int deleteNode(PrefixCollection** node, unsigned int base, char mask) {
    int status = 0;

    // Check if the node is empty
    if (NULL == *node) {
        //node not found
        status = -1;
    } else if (base < ((*node)->prefixes.base)) {
        // set the next node to search, left if given base is smaller than currently checking prefix base
        deleteNode(&((*node)->left), base, mask);
    } else if (base > ((*node)->prefixes.base)) {
        // set the next node to search, right if given base is greater than currently checking prefix base
        deleteNode(&((*node)->right), base, mask);
    } else {
        if (mask < ((*node)->prefixes.mask)) {
            // set the next node to search, left if given mask is smaller than currently checking prefix base
            deleteNode(&((*node)->left), base, mask);
        } else if (mask > ((*node)->prefixes.mask)) {
            // set the next node to search, right if given mask is greater than currently checking prefix base
            deleteNode(&((*node)->right), base, mask);
        } else {
            //node to delete found
            PrefixCollection* temp = *node;

            // check if node has children
            if (NULL == ((*node)->left)) {
                // no left children, replace node with right children
                *node = (*node)->right;
            } else if (NULL == ((*node)->right)) {
                // no right children, replace node with left children
                *node = (*node)->left;
            } else {
                // if node has both children
                PrefixCollection* minNode = (*node)->right;

                // find min node on right side
                while (NULL != (minNode->left)) {
                    minNode = minNode->left;
                }
                // replace node with min node from right side
                (*node)->prefixes = minNode->prefixes;
                // delete nim node from roght side
                deleteNode(&((*node)->right), minNode->prefixes.base, minNode->prefixes.mask);
            }

            free(temp);
        }
    }

    return status;
}
