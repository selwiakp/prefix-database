typedef struct {
    unsigned int base;
    char mask;
}
Prefix;

typedef struct PrefixCollection {
    Prefix prefixes;
    struct PrefixCollection * left;
    struct PrefixCollection * right;
}
PrefixCollection;

extern PrefixCollection * head;

int add(unsigned int base, char mask);
int del(unsigned int base, char mask);
char check(unsigned int ip);
int checkData(unsigned int base, char mask);
int checkPrefix(PrefixCollection* node, unsigned int base, char mask);
void printPrefixes(PrefixCollection* node);
void insertNode(PrefixCollection** node, unsigned int base, char mask);
int deleteNode(PrefixCollection** node, unsigned int base, char mask);
